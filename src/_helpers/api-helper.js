import axios from "axios";
import { settings } from "@/settings";

const API_SERVER = settings.API_SERVER;

export const api_routes = {
  user: {
    login: API_SERVER + "login"
  },
  contractor: {
    services: API_SERVER + "service",
    all: API_SERVER + "user?type_id=3",
    create: API_SERVER + "user",
    delete: API_SERVER + "user/",
    edit: API_SERVER + "user/",
    update: API_SERVER + "user/"
  },
  users: {
    roles: API_SERVER + "#",
    all: API_SERVER + "user?type_id=2",
    create: API_SERVER + "user",
    delete: API_SERVER + "user/",
    edit: API_SERVER + "user/",
    update: API_SERVER + "user/"
  },
  services: {
    all: API_SERVER + "service/",
    create: API_SERVER + "service",
    delete: API_SERVER + "service/",
    edit: API_SERVER + "service/",
    update: API_SERVER + "service/"
  },
  reclamations: {
    all: API_SERVER + "order/claim/",
    create: API_SERVER + "order/claim",
    delete: API_SERVER + "order/claim/",
    edit: API_SERVER + "order/claim/",
    update: API_SERVER + "order/claim/"
  },
  orders: {
    all: API_SERVER + "order",
    show: API_SERVER + "order/",
    services: API_SERVER + "service"
  },
  dashboard: {
    totalOrder: API_SERVER + "report/orders/total",
    avgTime: API_SERVER + "report/orders/duration",
    totalClaim: API_SERVER + "report/orders/claim",
    orderBySector: API_SERVER + "report/orders/sector?per_page=5",
    materials: API_SERVER + "report/materials/average",
    avgRating: API_SERVER + "report/users/rating/avg"
  },
  maintenance: {
    roles: {
      all: API_SERVER + "roles/",
      add: API_SERVER + "roles/",
      get: API_SERVER + "roles/",
      delete: API_SERVER + "roles/",
      update: API_SERVER + "roles/",
      addToUser: API_SERVER + "roles/assignRolToUser",
      removeFromUser: API_SERVER + "roles/removeRolFromUser"
    },
    permissions: {
      all: API_SERVER + "permissions/",
      addToRol: API_SERVER + "permissions/assignPermissionToRol",
      removeFromRol: API_SERVER + "permissions/removePermissionFromRol"
    },
    cancellationTypes: {
      all: API_SERVER + "cancellation/types"
    },
    cancellationReasons: {
      add: API_SERVER + "cancellation/reasons/",
      delete: API_SERVER + "cancellation/reasons/",
      update: API_SERVER + "cancellation/reasons/"
    },
    claimReasons: {
      all: API_SERVER + "order/claim/reasons/",
      add: API_SERVER + "order/claim/reasons/",
      delete: API_SERVER + "order/claim/reasons/",
      update: API_SERVER + "order/claim/reasons/"
    }
  }
};

export const apiCall = ({ url, method, ...args }) =>
  new Promise((resolve, reject) => {
    let token = localStorage.getItem("user-token") || "";

    if (token)
      axios.defaults.headers.common["Authorization"] = "Bearer " + token;

    try {
      axios({ method: method || "get", url: url, ...args })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    } catch (err) {
      reject(new Error(err));
    }
  });