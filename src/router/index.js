import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Todos from '../views/TodosView.vue'
import Register from '../views/RegisterView.vue'
import Login from '../views/LoginView.vue'
import Projects from '../views/Projects.vue'
import Articles from '../views/ArticlesView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/todos',
    name: 'TodosView',
    component: Todos
  },
  {
    path: '/register',
    name: 'RegisterView',
    component: Register
  },
  {
    path: '/login',
    name: 'LoginView',
    component: Login
  },
  {
    path: '/projects',
    name: 'Projects',
    component: Projects
  },
  {
    path: '/articles',
    name: 'ArticlesView',
    component: Articles
  }
]

const router = new VueRouter({
  routes
})

export default router

router.beforeEach((to, from, next) => {
  const publicPage = ['/login'];
  const authRequired = !publicPage.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if(authRequired && !loggedIn) {
    return next('/login');
  }

  next();
})
