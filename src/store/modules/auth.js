// import { authHeader } from '../../_helpers/auth-helper.js';
import axios from 'axios'

const getters = {}


const actions = {
    async login({ commit }, user) {
        const response = await axios.post(
            'http://127.0.0.1:8001/api/login', 
            {user}
        );

        localStorage.setItem('user', JSON.stringify(response.data));

        commit('newLogin', response.data);
    }

}

const mutations = {}

export default {
    getters,
    actions,
    mutations
}