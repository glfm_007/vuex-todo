import axios from 'axios'

const state = {
    articles: []
};

const getters = {
    allArticles: state => state.articles,
};

const actions = {
    async fetchArticles({ commit }) {
        const response = await axios.get(
            'http://127.0.0.1:8001/api/article'
        );

        commit('setArticles', response.data);
    },

    async addArticle({ commit }) {
        const response = await axios.post(
            'http://127.0.0.1:8001/api/article'
        );

        commit('newArticle', response.data);
    }
}

const mutations = {
    setArticles: (state, articles) => (state.articles = articles),
}


export default {
    state,
    getters,
    actions,
    mutations
}