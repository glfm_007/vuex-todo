// import { authHeader } from '../../_helpers/auth-helper.js';
import axios from 'axios';

// const headers = authHeader();


const state = {
    todos: []
};

const getters = {
    allTodos: state => state.todos
};

const actions = {
    async fetchTodos({ commit }) {
        const response = await axios.get(
            'http://127.0.0.1:8001/api/todo'
        );

        commit('setTodos', response.data);
    },

    async addTodo({ commit }, title) {
        const response = await axios.post(
            'http://127.0.0.1:8001/api/todo', 
            {title, completed: false}
        );

        commit('newTodo', response.data);
    },

    //Delete from de server
    async deleteTodo({ commit }, id) {
        await axios.delete(
        `http://127.0.0.1:8001/api/todo/${id}`
        );

        commit('removeTodo', id);
    },

    async filterTodos({ commit }, e) {

        //Get selected limit number
        const limit = parseInt(
            e.target.options[e.target.options.selectedIndex].innerText
        );

        //Get with the limit
        const response = await axios.get(
            `https://jsonplaceholder.typicode.com/todos?_limit=${limit}`
        );

        commit('setTodos', response.data);

    },

    async updateTodo({ commit }, updTodo) {
        const response = await axios.put(
            `http://127.0.0.1:8001/api/todo/${updTodo.id}`, updTodo
        );

        // console.log(response.data);

        commit('updateTodo', response.data);
    }
};

const mutations = {

    //Set the todos on the UI
    setTodos: (state, todos) => (state.todos = todos),

    //Add new todo to UI array
    newTodo: (state, todo) => state.todos.unshift(todo),

    //Delete from the UI
    removeTodo: (state, id) => 
        (state.todos = state.todos.filter(todo => todo.id !== id)),

    //Update Todo
    updateTodo: (state, updTodo) => {
        const index = state.todos.findIndex(todo => todo.id === updTodo.id);

        if(index !== -1) {
            state.todos.splice(index, 1, updTodo);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}