import Vuex from 'vuex';
import Vue from 'vue';
import todos from './modules/todos';
import articles from './modules/articles';
import auth from './modules/auth';

//Load Vuex
Vue.use(Vuex);

//Create store
export default new Vuex.Store({
    modules: {
        auth,
        todos,
        articles
    }
});